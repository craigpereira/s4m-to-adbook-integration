﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Canwest.Broadcasting.Windows.Classes.CW_Common_2008;
using AdBook_DigitalClients_Integrator.AdBook_API;
using System.Data.SqlClient;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace AdBook_DigitalClients_Integrator.x
{
    class AdBook_DigitalClients_Integrator
    {
        Logging oLogger;

        private const string QUERY_GET_TEMPTABLE_DATA = "SELECT TOP 10 Agency_Name, Advertiser_Name, Agency_ID, Advertiser_ID, DFP_ID, SalesForce_ID, Client_Type, Bookable FROM Corporate_Applications..S4M_Agency_Advertiser_for_AdBook";
        public AdBook_DigitalClients_Integrator(Logging Logger)
        {
            oLogger = Logger;
        }

        public void Run(string UserName, string Password)
        {
            try
            {
                using (AdBookConnectClient adbkService = new AdBookConnectClient())
                {
                    // creates the service client and adds the credentials to the header
                    #region Setup AdBook Environment URLs

                    string svcUrl = "";

                    // production Login URL differs from the Sandbox Login URL, so use correct one depending on environment
                    if (Framework.AppMode == enAppMode.enProduction)
                    {
                        svcUrl = "https://adbook03.fattail.com/abn/WS/AdBookConnect.svc";
                    }
                    else
                    {
                        svcUrl = "https://sandbox03.fattail.com/abn/WS/AdBookConnect.svc";
                    }

                    adbkService.Endpoint.Address = new EndpointAddress(svcUrl);

                    if (adbkService.ClientCredentials != null)
                    {
                        adbkService.ClientCredentials.UserName.UserName = UserName;
                        adbkService.ClientCredentials.UserName.Password = Password;
                    }

                    oLogger.WriteLog("Logging into the following AdBook API: " + adbkService.Endpoint.Address);

                    if (System.Net.ServicePointManager.SecurityProtocol == (SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls))
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                    #endregion

                    #region Login to AdBook and perform Insert/Update
                    using (OperationContextScope scope = new OperationContextScope(adbkService.InnerChannel))  // logs into AdBook (creates the OperationContextScope and adds the api version number to the header)
                    {
                        MessageHeader header = MessageHeader.CreateHeader("Version", "http://www.FatTail.com/api", 24);
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);



                        DAL oDal = new DAL();

                        // get the list of S4M updates from temp table
                        DataTable oTable = oDal.ExecuteDataTable(QUERY_GET_TEMPTABLE_DATA);

                        oLogger.WriteLog(oTable.Rows.Count + " S4M updates were found in the database.");


                        // get list of Advertisers from AdBook
                        List<Client> listOfClients = new List<Client>();
                        listOfClients = adbkService.GetClientList().ToList();

                        // get list of Agencies from AdBook
                        List<Agency> listOfAgencies = new List<Agency>();
                        listOfAgencies = adbkService.GetAgencyList().ToList();

                        // ClientAgencyRelationships
                        HashSet<Tuple<long, long>> hashSet = new HashSet<Tuple<long, long>>();

                        var v1 = new Tuple<long, long>(1234, 3513514);
                        var v2 = new Tuple<long, long>(1234, 242425);
                        var v3 = new Tuple<long, long>(1234, 33434);
                        var v4 = new Tuple<long, long>(2345, 353535);
                        hashSet.Add(v1);
                        hashSet.Add(v2);
                        hashSet.Add(v3);
                        hashSet.Add(v4);
                        hashSet.Add(v1);
                        hashSet.Add(v4);



                        /****** Ad Server Mapping - IMPLEMENT THIS IF NEW AdServerID IS ADDED
                        AdServer[] adServerList = adbkService.GetAdServerList();
                        long adServerID;

                        foreach (AdServer adServerDFP in adServerList)
                        {
                            if (adServerDFP.Name == "DFP Premium")
                                adServerID = adServerDFP.AdServerID; // right now only AdServerID set up is 233
                        }
                        /*

                        /****** Ad Server Mapping*/
                        var mappingList = adbkService.GetAccountAdServerMappingsList();
                        
                        int number_of_mappings;

                        AccountAdServerMapping[] mappings = new AccountAdServerMapping[number_of_mappings];

                        mappings[0].AccountID =  //accountID from temp table
                        mappings.AdServerEntityID = 
                        mappings.AdServerID = //value from GetAccountAdServerMappingsList()

                        adbkService.UpdateAccountAdServerMappings(mappings);
                        

                        Agency newAgency = new Agency();

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow oRow in oTable.Rows)
                            {
                                if ((oRow["Client_Type"].ToString() == "Agency"))                                                                   // Client Type
                                {
                                    Agency agency = new Agency();
                                    agency.Name = oRow["Agency_Name"].ToString();                                                                   // Client Name
                                    agency.CustomerCode = oRow["Agency_ID"].ToString();                                                             // S4M ID
                                    agency.CRMID = oRow["SalesForce_ID"].ToString();                                                                // Salesforce ID

                                    if (oRow["Bookable"].ToString() == "Blocked Customer" || oRow["Bookable"].ToString() == "Critical Customer")    // Credit Risk
                                        agency.IsCreditRisk = true;
                                    else                                // oRow["Bookable"].ToString() == "Standard Customer"
                                        agency.IsCreditRisk = false;

                                    // CreateAgency() returns an Agency object with new AgencyID
                                    newAgency = adbkService.CreateAgency(agency);

                                    AgencyContact agencyContact = new AgencyContact();
                                    agencyContact.AgencyID = newAgency.AgencyID;    // use the AgencyID of the newly created agency to associate with the AgencyContact
                                    agencyContact.LastName = "Please See";
                                    agencyContact.FirstName = "Great Plains";

                                    adbkService.CreateAgencyContact(agencyContact);



                                    //if TT Agency_ID exists in listOfAgencies (lookup of Agency.AgencyID)
                                    //then UpdateAgency()
                                    //else
                                    //CreateAgency()
                                }
                                else // (oRow["Client_Type"].ToString() == "Advertiser")
                                {
                                    //if TT Advertiser_ID exist in listOfClients (lookup of Client.ClientID)
                                        //then UpdateClient()
                                    //else
                                        //CreateClient()
                                }
                            }
                        }

                    #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                oLogger.WriteLog("The error returned was " + ex.ToString());
            }
        }

    }
}
