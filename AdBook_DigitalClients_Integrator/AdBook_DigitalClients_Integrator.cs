﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Canwest.Broadcasting.Windows.Classes.CW_Common_2008;
using AdBook_DigitalClients_Integrator.AdBook_API;
using System.Data.SqlClient;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace AdBook_DigitalClients_Integrator.x
{
    class AdBook_DigitalClients_Integrator
    {
        Logging oLogger;

        private const string QUERY_GET_TEMPTABLE_DATA = "SELECT ADB_ID, Agency_Name, Advertiser_Name, Agency_ID, Advertiser_ID, DFP_ID, SalesForce_ID, Client_Type, Bookable, Processed FROM Corporate_Applications..S4M_Agency_Advertiser_for_AdBook WHERE Processed='0'";
        private const string UPDATE_PROCESSED_FLAG = "UPDATE Corporate_Applications..S4M_Agency_Advertiser_for_AdBook SET Processed = 1 WHERE ADB_ID IN ({0})";

        public AdBook_DigitalClients_Integrator(Logging Logger)
        {
            oLogger = Logger;
        }

        public void Run(string username, string passwd)
        {
            try
            {
                using (AdBookConnectClient adbkService = new AdBookConnectClient())
                {
                    // creates the service client and adds the credentials to the header
                    #region Setup AdBook Environment URLs

                    string svcUrl = "";

                    // production Login URL differs from the Sandbox Login URL, so use correct one depending on environment
                    if (Framework.AppMode == enAppMode.enProduction)
                    {
                        svcUrl = "https://adbook03.fattail.com/abn/WS/AdBookConnect.svc";
                    }
                    else
                    {
                        svcUrl = "https://sandbox03.fattail.com/abn/WS/AdBookConnect.svc";
                    }

                    adbkService.Endpoint.Address = new EndpointAddress(svcUrl);

                    if (adbkService.ClientCredentials != null)
                    {
                        adbkService.ClientCredentials.UserName.UserName = username;
                        adbkService.ClientCredentials.UserName.Password = passwd;
                    }

                    oLogger.WriteLog("Logging into the following AdBook API: " + adbkService.Endpoint.Address);

                    if (System.Net.ServicePointManager.SecurityProtocol == (SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls))
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                    #endregion

                    #region Login to AdBook and perform Insert/Update
                    using (OperationContextScope scope = new OperationContextScope(adbkService.InnerChannel))  // logs into AdBook (creates the OperationContextScope and adds the api version number to the header)
                    {
                        MessageHeader header = MessageHeader.CreateHeader("Version", "http://www.FatTail.com/api", 24);
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);

                        adbkService.InnerChannel.OperationTimeout = TimeSpan.FromSeconds(300);  // was running into timeout errors at 59s so increased the default

                        // get list of Advertisers from AdBook and store in a dictionary object
                        List<Client> clientList = adbkService.GetClientList();
                        Dictionary<string, Client> clientCustomerCodeLookup = clientList.ToDictionary(l => !string.IsNullOrEmpty(l.CustomerCode) ? l.CustomerCode : l.Name);

                        // get list of Agencies from AdBook and store in a dictionary object
                        List<Agency> agencyList = adbkService.GetAgencyList();
                        Dictionary<string, Agency> agencyCustomerCodeLookup = agencyList.ToDictionary(a => !string.IsNullOrEmpty(a.CustomerCode) ? a.CustomerCode : a.Name);

                        List<DataRow> agencyRows = new List<DataRow>();
                        List<DataRow> advertiserRows = new List<DataRow>();
                        List<string> listADB_IDs = new List<string>();      // build a list of ADB_IDs to set Processed flag to 1 in temp table once successfully uploaded to AdBook

                        // get the list of S4M updates from temp table
                        DAL oDal = new DAL();
                        DataTable oTable = oDal.ExecuteDataTable(QUERY_GET_TEMPTABLE_DATA);
                        oLogger.WriteLog(oTable.Rows.Count + " S4M updates were found in the database.");

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow oRow in oTable.Rows)
                            {
                                if (!((oRow["Agency_ID"].ToString() == oRow["Advertiser_ID"].ToString()) && (oRow["Client_Type"].ToString() == "Advertiser"))) // ignore direct booking line from temp table (where Agency = Advertiser, and Client_Type = Advertiser)
                                {
                                    if (oRow["Client_Type"].ToString() == "Advertiser")
                                    {
                                        advertiserRows.Add(oRow);
                                        listADB_IDs.Add(oRow["ADB_ID"].ToString());
                                    }
                                    else
                                    {
                                        agencyRows.Add(oRow);
                                        listADB_IDs.Add(oRow["ADB_ID"].ToString());
                                    }

                                }
                                else
                                {
                                    listADB_IDs.Add(oRow["ADB_ID"].ToString());  // set Processed flag to '1' even when Agency = Advertiser, and Client_Type = Advertiser
                                }
                            }
                        }

                        Lookup<long, AccountAdServerMapping> adserverMappings = (Lookup<long, AccountAdServerMapping>)adbkService.GetAccountAdServerMappingsList().ToLookup(m => m.AccountID);
                        PushAgenciesToAdBook(adbkService, agencyCustomerCodeLookup, agencyRows, adserverMappings);
                        PushAdvertisersToAdBook(adbkService, clientCustomerCodeLookup, advertiserRows, agencyCustomerCodeLookup, adserverMappings);
                        UpdateTempTableFlag(listADB_IDs);

                        #endregion
                    }
                }
            }
            catch (FaultException<DataAccessFault> ex) // catch the error that says "The creator of this fault did not specify a Reason." and get more detail
            {
                int item = 0;
                foreach (var detail in ex.Detail.FaultMessges)
                {
                    if (detail != String.Empty)
                    {
                        oLogger.WriteLog("The error returned was [" + item + "]: " + detail);
                        item++;
                    }
                }
            }
            catch (Exception ex)
            {
                oLogger.WriteLog("The error returned was " + ex.ToString());
            }

        }

        public void PushAgenciesToAdBook(AdBookConnectClient adbkService, Dictionary<string, Agency> agencyCustomerCodeLookup, List<DataRow> agencyRows, Lookup<long, AccountAdServerMapping> adserverMappings)
        {
            foreach (DataRow ar in agencyRows)
            {
                string s4mAgencyID = ar["Agency_ID"].ToString();
                Agency abAgency;

                if (!agencyCustomerCodeLookup.TryGetValue(s4mAgencyID, out abAgency) && !agencyCustomerCodeLookup.TryGetValue(ar["Agency_Name"].ToString(), out abAgency))
                {
                    abAgency = new Agency();
                }

                abAgency.Name = ar["Agency_Name"].ToString();
                abAgency.CustomerCode = ar["Agency_ID"].ToString();
                abAgency.CRMID = ar["SalesForce_ID"].ToString();

                if (ar["Bookable"].ToString() == "Blocked Customer" || ar["Bookable"].ToString() == "Critical Customer")
                {
                    abAgency.IsCreditRisk = true;
                }
                else
                {
                    abAgency.IsCreditRisk = false;
                }

                if (abAgency.AgencyID == 0)  // if we just created the agency, then create the agency contact too
                {
                    abAgency = adbkService.CreateAgency(abAgency);
                    AgencyContact agencyContact = new AgencyContact();
                    agencyContact.AgencyID = abAgency.AgencyID;
                    agencyContact.LastName = "Please See";
                    agencyContact.FirstName = "Great Plains";
                    adbkService.CreateAgencyContact(agencyContact);
                    agencyCustomerCodeLookup.Add(abAgency.CustomerCode, abAgency);
                }
                else
                {
                    adbkService.UpdateAgency(abAgency);
                }

                DFPMapping(adbkService, abAgency.AgencyID, ar["DFP_ID"].ToString(), adserverMappings);
            }
        }

        public void PushAdvertisersToAdBook(AdBookConnectClient adbkService, Dictionary<string, Client> clientCustomerCodeLookup, List<DataRow> advertiserRows, Dictionary<string, Agency> agencyCustomerCodeLookup, Lookup<long, AccountAdServerMapping> adserverMappings)
        {
            IEnumerable<Tuple<long, long>> rel = adbkService.GetClientAgencyRelationships().Select(r => new Tuple<long, long>(r.ClientID, r.AgencyID));
            HashSet<Tuple<long, long>> existingClientAgencyRelationships = new HashSet<Tuple<long, long>>(rel);

            foreach (DataRow ar in advertiserRows)
            {
                string s4mAgencyID = ar["Advertiser_ID"].ToString();
                Client abClient;

                if (!clientCustomerCodeLookup.TryGetValue(s4mAgencyID, out abClient) && !clientCustomerCodeLookup.TryGetValue(ar["Advertiser_Name"].ToString(), out abClient))
                {
                    abClient = new Client();
                }

                abClient.Name = ar["Advertiser_Name"].ToString();
                abClient.CustomerCode = ar["Advertiser_ID"].ToString();
                abClient.CRMID = ar["SalesForce_ID"].ToString();

                if (ar["Bookable"].ToString() == "Blocked Customer" || ar["Bookable"].ToString() == "Critical Customer")
                {
                    abClient.IsCreditRisk = true;
                }
                else
                {
                    abClient.IsCreditRisk = false;
                }

                if (abClient.ClientID == 0)
                {
                    abClient = adbkService.CreateClient(abClient);
                    clientCustomerCodeLookup.Add(abClient.CustomerCode, abClient);
                }
                else
                {
                    adbkService.UpdateClient(abClient);
                }

                CreateClientAgencyRelationships(adbkService, ar, clientCustomerCodeLookup, agencyCustomerCodeLookup, existingClientAgencyRelationships);

                DFPMapping(adbkService, abClient.ClientID, ar["DFP_ID"].ToString(), adserverMappings);
            }
        }

        public void CreateClientAgencyRelationships(AdBookConnectClient adbkService, DataRow ar, Dictionary<string, Client> clientCustomerCodeLookup, Dictionary<string, Agency> agencyCustomerCodeLookup, HashSet<Tuple<long, long>> existingClientAgencyRelationships)
        {
            Agency abAgency;
            if (agencyCustomerCodeLookup.TryGetValue(ar["Agency_ID"].ToString(), out abAgency))
            {
                Client abClient;
                if (clientCustomerCodeLookup.TryGetValue(ar["Advertiser_ID"].ToString(), out abClient))
                {
                    if (!existingClientAgencyRelationships.Contains(new Tuple<long, long>(abClient.ClientID, abAgency.AgencyID)))
                    {
                        ClientAgencyRelationship clientAgencyRelationship = new ClientAgencyRelationship();
                        clientAgencyRelationship.ClientID = abClient.ClientID;
                        clientAgencyRelationship.AgencyID = abAgency.AgencyID;
                        adbkService.CreateClientAgencyRelationship(clientAgencyRelationship);
                    }
                }
            }
        }

        public void DFPMapping(AdBookConnectClient adbkService, long accountID, string dfpID, Lookup<long, AccountAdServerMapping> adserverMappings)
        {
            const long AdServerID = 233;  // probably a bad idea to hard code this!

            AccountAdServerMapping accountMapping = default(AccountAdServerMapping);
            if (adserverMappings.Contains(accountID))
            {
                accountMapping = adserverMappings[accountID].FirstOrDefault(m => m.AdServerID == AdServerID);
            }
            if (accountMapping == default(AccountAdServerMapping))
            {
                accountMapping = new AccountAdServerMapping();
                accountMapping.AccountID = accountID;
                accountMapping.AdServerID = AdServerID;
                accountMapping.AdServerEntityID = dfpID;
                adbkService.CreateAccountAdServerMappings(new List<AccountAdServerMapping>() { accountMapping });
            }
            else if (accountMapping.AdServerEntityID != dfpID)
            {
                accountMapping.AdServerEntityID = dfpID;
                adbkService.UpdateAccountAdServerMappings(new List<AccountAdServerMapping>() { accountMapping });
            }
        }

        public void UpdateTempTableFlag(List<string> successfullyUpdatedIDs)
        {
            if (successfullyUpdatedIDs.Count > 0)
            {
                DAL oDal = new DAL();

                var updated_ADB_IDs = String.Join(",", successfullyUpdatedIDs.ToArray());
                oDal.ExecuteNonQuery(String.Format(UPDATE_PROCESSED_FLAG, updated_ADB_IDs));
                oLogger.WriteLog(successfullyUpdatedIDs.Count + " ADB_IDs were successfully updated and those Processed flags will be set to 1.");
            }
        }

    }
}
