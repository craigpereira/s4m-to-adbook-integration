﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Canwest.Broadcasting.Windows.Classes.CW_Common_2008;

namespace AdBook_DigitalClients_Integrator.x
{
    class EmailHandler
    {
        private string _EmailAddress;

        public EmailHandler()
        {
            // Look in machine.config for an email address, else default to the dev team
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SalesForceIntegratorErrorEmail"]))
            {
                _EmailAddress = ConfigurationManager.AppSettings["SalesForceIntegratorErrorEmail"];
            }
            else
            {
                _EmailAddress = "corusit-enterpriseapplicationdevelopment@corusent.com";
            }
        }

        public void SendExceptionEmail(Exception ex)
        {
            // Mail the exception
            MailMessage oMessage = new MailMessage();
            foreach (string emailAddr in _EmailAddress.Split(';'))
            {
                if (!String.IsNullOrEmpty(emailAddr)) {
                    oMessage.To.Add(new MailAddress(emailAddr));
                }
            }
            oMessage.From = new MailAddress("corusit-enterpriseapplicationdevelopment@corusent.com");
            oMessage.Subject = "Critical Exception in SalesForce MonthlySummary Integrator";
            oMessage.IsBodyHtml = true;

            oMessage.Body = String.Format("<b>A Critical Exception was encountered in SalesForce MonthlySummary Integrator - {0}</b><br/>{1}", Framework.AppMode, ex.Message);

            Framework.SendMailMessage(oMessage);
        }
    }
}
