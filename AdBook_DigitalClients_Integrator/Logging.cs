﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Canwest.Broadcasting.Windows.Classes.CW_Common_2008;

namespace AdBook_DigitalClients_Integrator.x
{
    delegate void LogMessage(string inString);

    class Logging
    {
        Logger oLogger;
        LogMessage messageHandler;

        public enum LogType {
            Console,
            Database
        }

        private LogType _logType;

        public LogType LoggingType
        {
            get
            {
                return _logType;
            }
            set
            {
                _logType = value;

                if (value == LogType.Console)
                {
                    messageHandler = WriteToConsole;
                }
                else
                {
                    messageHandler = WriteToDatabase;
                }
            }
        }

        // Default to Write to Console if the app is in dev, otherwise write to the database
        public Logging()
        {
            oLogger = new Logger();
            oLogger.Application_ID = 158;
            oLogger.Log_Destination_Type = Logger.enLogDestinationType.idatabase;

            if (Framework.AppMode == enAppMode.enDevelopment)
            {
                LoggingType = LogType.Console;
            }
            else
            {
                LoggingType = LogType.Database;
            }
        }

        // Allow the user to define where they want the logging to go.
        public Logging(LogType type)
        {
            oLogger = new Logger();
            oLogger.Application_ID = 158;
            oLogger.Log_Destination_Type = Logger.enLogDestinationType.idatabase;

            LoggingType = type;
        }

        // The main method of writing the messages
        public void WriteLog(string inString)
        {
            messageHandler(inString);
        }

        // Write messages to console
        private void WriteToConsole(string inString)
        {
            Console.WriteLine(inString);
        }

        // Write messages to the AAC_Logger database
        private void WriteToDatabase(string inString)
        {
            oLogger.WriteLog(inString);
        }

        internal void WriteLog(object p)
        {
            throw new NotImplementedException();
        }
    }
}
