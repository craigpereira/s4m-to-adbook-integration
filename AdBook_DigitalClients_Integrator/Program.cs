﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdBook_DigitalClients_Integrator.AdBook_API;

namespace AdBook_DigitalClients_Integrator.x
{
    class Program
    {
        static void Main(string[] args)
        {
            Logging oLogger = new Logging();

            oLogger.WriteLog("Starting S4M AdBook Integrator at " + DateTime.Now.ToString("MMM dd, hh:mm:ss"));

            string UserName = "", Password = "";

            try
            {
                // Username and password must be passed as command arguments
                if (args.Length >= 2)
                {
                    UserName = args[0];
                    Password = args[1];

                    AdBook_DigitalClients_Integrator adbk_int = new AdBook_DigitalClients_Integrator(oLogger);
                    adbk_int.Run(UserName, Password);
                }
                else
                {
                    oLogger.WriteLog("UserName and Password are not defined as command arguments.  Stopping the application.");
                    throw new Exception("UserName and Password are not defined as command arguments.  Stopping the application.");
                }
            }
            catch (Exception ex)
            {
                oLogger.WriteLog("An unhandled exception was encountered: " + ex.ToString());
                // Only critical exceptions are thrown.  Email the error to the development team to rectify the issue.
                EmailHandler oHandler = new EmailHandler();
                oHandler.SendExceptionEmail(ex);
            }

            oLogger.WriteLog("S4M AdBook Integrator finished execution at " + DateTime.Now.ToString("MMM dd, hh:mm:ss"));

            // If logging to the console, allow the user to read the messages before closing the app.
            if (oLogger.LoggingType == Logging.LogType.Console)
            {
                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }
        }
    }
}
