﻿void Main()
{
    DataTable tempTable = GetTempTable();

    using (AdBookConnectClient service = new AdBookConnectClient())
    {
        string svcUrl = "https://sandbox03.fattail.com/abn/WS/AdBookConnect.svc";
        service.Endpoint.Address = new EndpointAddress(svcUrl);

        if (service.ClientCredentials != null)
        {
            service.ClientCredentials.UserName.UserName = "myusername";
            service.ClientCredentials.UserName.Password = "mypassword";
        }
        using (OperationContextScope scope = new OperationContextScope(service.InnerChannel))
        {
            MessageHeader header = MessageHeader.CreateHeader("Version", "http://www.FatTail.com/api", 24);
            OperationContext.Current.OutgoingMessageHeaders.Add(header);

            // Call web service methods here.
            try
            {
                List<Client> clientList = service.GetClientList();
                Dictionary<string, Client> clientCustomerCodeLookup = clientList.ToDictionary(l => !string.IsNullOrEmpty(l.CustomerCode) ? l.CustomerCode : l.Name);

                List<Agency> agencyList = service.GetAgencyList();
                Dictionary<string, Agency> agencyCustomerCodeLookup = agencyList.ToDictionary(a => !string.IsNullOrEmpty(a.CustomerCode) ? a.CustomerCode : a.Name);

                List<DataRow> agencyRows = new List<DataRow>();
                List<DataRow> advertiserRows = new List<DataRow>();
                foreach (DataRow dr in tempTable.Rows)
                {
                    if (dr["Client_Type"].ToString() == "Advertiser")
                    {
                        advertiserRows.Add(dr);
                    }
                    else
                    {
                        agencyRows.Add(dr);
                    }
                }

                Lookup<long, AccountAdServerMapping> adserverMappings = (Lookup<long, AccountAdServerMapping>)service.GetAccountAdServerMappingsList().ToLookup(m => m.AccountID);

                PersistAgenciesToAdBook(service, agencyCustomerCodeLookup, agencyRows, adserverMappings);

                PersistAdvertisersToAdBook(service, clientCustomerCodeLookup, advertiserRows, agencyCustomerCodeLookup, adserverMappings);
            }
            catch (Exception ex)
            {
                ex.Dump();
            }
        }
    }

    tempTable.Dump();
}

// Define other methods and classes here

public DataTable GetTempTable()
{
    DataTable tempTable = new DataTable();
    tempTable.Columns.Add("Agency_Name", typeof(string));
    tempTable.Columns.Add("Advertiser_Name", typeof(string));
    tempTable.Columns.Add("Agency_ID", typeof(long));
    tempTable.Columns.Add("Advertiser_ID", typeof(long));
    tempTable.Columns.Add("DFP_ID", typeof(string));
    tempTable.Columns.Add("SalesForce_ID", typeof(string));
    tempTable.Columns.Add("Client_Type", typeof(string));
    tempTable.Columns.Add("Bookable", typeof(string));

    tempTable.Rows.Add("Agency 1", string.Empty, 10001, 0, "31001", "41001", "Agency", "Standard Customer");
    tempTable.Rows.Add("Agency 1", "Advertiser 2", 10001, 20002, "30002", "40002", "Advertiser", "Standard Customer");
    tempTable.Rows.Add("Agency 1", "Advertiser 3", 10001, 20003, "30003", "40003", "Advertiser", "Blocked Customer");
    tempTable.Rows.Add("Agency 2", string.Empty, 10002, 0, "31002", "41002", "Agency", "Standard Customer");
    tempTable.Rows.Add("Agency 2", "Advertiser 4", 10002, 20004, "30004", "40004", "Advertiser", "Standard Customer");
    tempTable.Rows.Add("Agency 2", "Advertiser 5", 10002, 20005, "30005", "40005", "Advertiser", "Critical Customer");
    tempTable.Rows.Add("Agency 2", "Advertiser 6", 10002, 20006, "30006", "40006", "Advertiser", "Standard Customer");
    tempTable.Rows.Add("Agency 3", string.Empty, 10003, 0, "31007", "41007", "Agency", "Standard Customer");
    tempTable.Rows.Add("Agency 3", "Advertiser 7", 10003, 20007, "30007", "40007", "Advertiser", "Standard Customer");

    return tempTable;
}

public void PersistAgenciesToAdBook(AdBookConnectClient service, Dictionary<string, Agency> agencyCustomerCodeLookup, List<DataRow> agencyRows, Lookup<long, AccountAdServerMapping> adserverMappings)
{
    foreach (DataRow ar in agencyRows)
    {
        string s4mAgencyID = ar["Agency_ID"].ToString();
        Agency abAgency;
        if (!agencyCustomerCodeLookup.TryGetValue(s4mAgencyID, out abAgency)
               && !agencyCustomerCodeLookup.TryGetValue(ar["Agency_Name"].ToString(), out abAgency))
        {
            abAgency = new Agency();
        }
        abAgency.Name = ar["Agency_Name"].ToString();
        abAgency.CustomerCode = ar["Agency_ID"].ToString();
        abAgency.CRMID = ar["SalesForce_ID"].ToString();
        if (ar["Bookable"].ToString() == "Blocked Customer" || ar["Bookable"].ToString() == "Critical Customer")
        {
            abAgency.IsCreditRisk = true;
        }
        else
        {
            abAgency.IsCreditRisk = false;
        }
        if (abAgency.AgencyID == 0)  // if we just created the agency, then create the agency contact too
        {
            abAgency = service.CreateAgency(abAgency);
            AgencyContact agencyContact = new AgencyContact();
            agencyContact.AgencyID = abAgency.AgencyID;
            agencyContact.LastName = "Please See";
            agencyContact.FirstName = "Great Plains";
            service.CreateAgencyContact(agencyContact);
            agencyCustomerCodeLookup.Add(abAgency.CustomerCode, abAgency);
        }
        else
        {
            service.UpdateAgency(abAgency);
        }

        PersistDFPMapping(service, abAgency.AgencyID, ar["DFP_ID"].ToString(), adserverMappings);
    }
}

public void PersistAdvertisersToAdBook(AdBookConnectClient service, Dictionary<string, Client> clientCustomerCodeLookup, List<DataRow> advertiserRows, Dictionary<string, Agency> agencyCustomerCodeLookup, Lookup<long, AccountAdServerMapping> adserverMappings)
{
    HashSet<Tuple<long, long>> existingClientAgencyRelationships = service.GetClientAgencyRelationships().Select(r => new Tuple<long, long>(r.ClientID, r.AgencyID)).ToHashSet();

    foreach (DataRow ar in advertiserRows)
    {
        string s4mAgencyID = ar["Advertiser_ID"].ToString();
        Client abClient;
        if (!clientCustomerCodeLookup.TryGetValue(s4mAgencyID, out abClient)
               && !clientCustomerCodeLookup.TryGetValue(ar["Advertiser_Name"].ToString(), out abClient))
        {
            abClient = new Client();
        }
        abClient.Name = ar["Advertiser_Name"].ToString();
        abClient.CustomerCode = ar["Advertiser_ID"].ToString();
        abClient.CRMID = ar["SalesForce_ID"].ToString();
        if (ar["Bookable"].ToString() == "Blocked Customer" || ar["Bookable"].ToString() == "Critical Customer")
        {
            abClient.IsCreditRisk = true;
        }
        else
        {
            abClient.IsCreditRisk = false;
        }
        if (abClient.ClientID == 0)
        {
            abClient = service.CreateClient(abClient);
            clientCustomerCodeLookup.Add(abClient.CustomerCode, abClient);
        }
        else
        {
            service.UpdateClient(abClient);
        }

        CreateClientAgencyRelationships(service, ar, clientCustomerCodeLookup, agencyCustomerCodeLookup, existingClientAgencyRelationships);

        PersistDFPMapping(service, abClient.ClientID, ar["DFP_ID"].ToString(), adserverMappings);
    }
}

public void CreateClientAgencyRelationships(AdBookConnectClient service, DataRow ar, Dictionary<string, Client> clientCustomerCodeLookup, Dictionary<string, Agency> agencyCustomerCodeLookup, HashSet<Tuple<long, long>> existingClientAgencyRelationships)
{
    Agency abAgency;
    if (agencyCustomerCodeLookup.TryGetValue(ar["Agency_ID"].ToString(), out abAgency))
    {
        Client abClient;
        if (clientCustomerCodeLookup.TryGetValue(ar["Advertiser_ID"].ToString(), out abClient))
        {
            if (!existingClientAgencyRelationships.Contains(new Tuple<long, long>(abClient.ClientID, abAgency.AgencyID)))
            {
                ClientAgencyRelationship clientAgencyRelationship = new ClientAgencyRelationship();
                clientAgencyRelationship.ClientID = abClient.ClientID;
                clientAgencyRelationship.AgencyID = abAgency.AgencyID;
                service.CreateClientAgencyRelationship(clientAgencyRelationship);
            }
        }
    }
}

public void PersistDFPMapping(AdBookConnectClient service, long accountID, string dfpID, Lookup<long, AccountAdServerMapping> adserverMappings)
{
    const long AdServerID = 233;  // probably a bad idea to hard code this!

    AccountAdServerMapping accountMapping = default(AccountAdServerMapping);
    if (adserverMappings.Contains(accountID))
    {
        accountMapping = adserverMappings[accountID].FirstOrDefault(m => m.AdServerID == AdServerID);
    }
    if (accountMapping == default(AccountAdServerMapping))
    {
        accountMapping = new AccountAdServerMapping();
        accountMapping.AccountID = accountID;
        accountMapping.AdServerID = AdServerID;
        accountMapping.AdServerEntityID = dfpID;
        service.CreateAccountAdServerMappings(new List<AccountAdServerMapping>() { accountMapping });
    }
    else if (accountMapping.AdServerEntityID != dfpID)
    {
        accountMapping.AdServerEntityID = dfpID;
        service.UpdateAccountAdServerMappings(new List<AccountAdServerMapping>() { accountMapping });
    }
}
